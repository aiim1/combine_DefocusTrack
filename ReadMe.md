# Code for the paper: "Non-invasive Measurement of Wall Shear Stress in Microfluidic Chip for Osteoblast Cell Culture using Improved Depth Estimation of Defocus Particle Tracking Method"

Authors: Hein Htet Aung, Phattarin Pothipan, Jirasin Aswakool, Siraphob Santironnarong, Rungreang Phatthanakkun, Visarute Pinrod, Thanakorn Jiemsakul, Wares Chancharoen, Aekkacha Moonwiriyakit

## Description
This project provides MATLAB code to implement an improved defocus particle tracking method for non-invasive measurement of Wall Shear Stress (WSS) in microfluidic channels. Our method enhances the General Defocus Particle Tracking (GDPT) algorithm, which estimates 3D particle motion using conventional 2D imaging from a single digital camera and microscope.

The original GDPT algorithm has limitations, such as errors in estimating the direction of displacement for out-of-focus particles and the need for manual scaling to convert estimated depth values into physical units. Our approach overcomes these limitations by:

Leveraging theoretical knowledge of laminar flow,
Integrating results from multiple analyses, including Computational Fluid Dynamics (CFD) simulations and experimental video recordings.
The approach was validated using videos recorded from a microfluidic chip that generates different WSS levels under steady-state flow conditions, specifically for osteoblast cell culture applications.

## Usage
Download the repository and ensure you have MATLAB installed.
Open the combine_DefocusTrack.mlx file in MATLAB.
The MATLAB notebook will prompt you to select a folder containing example data. Download the "Example Data" folder from this repository and select it when prompted.
This code calculates WSS based on particle displacement data, using both experimental and CFD data to provide improved depth estimation.

## Citation
If you use this code or methodology in your research, please cite our paper:

Hein Htet Aung, Phattarin Pothipan, Jirasin Aswakool, Siraphob Santironnarong, Rungreang Phatthanakkun, Visarute Pinrod, Thanakorn Jiemsakul, Wares Chancharoen, Aekkacha Moonwiriyakit, "Non-invasive Measurement of Wall Shear Stress in Microfluidic Chip for Osteoblast Cell Culture using Improved Depth Estimation of Defocus Particle Tracking Method," Biomicrofluidics 18, 054114 (2024). DOI: https://doi.org/10.1063/5.0226294.

## License
This project is licensed under the GNU General Public License v3.0 (GPL-3.0). You are free to use, modify, and distribute this code, provided that any derivative works also adhere to the GPL-3.0 license terms.

combine_DefocusTrack - Code for paper "Non-invasive Measurement of Wall Shear Stress in Microfluidic Chip for Osteoblast Cell Culture using Improved Depth Estimation of Defocus Particle Tracking Method"

Copyright (C) 2024 Hein Htet Aung, Phattarin Pothipan, Jirasin Aswakool, Siraphob Santironnarong, Rungreang Phatthanakkun, Visarute Pinrod, Thanakorn Jiemsakul, Wares Chancharoen, Aekkacha Moonwiriyakit

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

## Acknowledgments
This study was supported by Chulabhorn Royal Academy [fundamental fund: fiscal year 2023 by National Science Research and Innovation Fund (FRB660044/0240; Project code No. 180851)]. The authors express their sincere gratitude to the Synchrotron Light Research Institute, Thailand, for the provision of facilities and assistance in chip design and fabrication and to Miss Sirilak Phetcharat for her invaluable support to this study. We thank Edanz for editing a draft of this manuscript.

This project builds on the GDPT algorithm framework and benefits from integration with CFD simulations to validate depth estimation techniques. We thank our collaborators and the broader research community for their contributions.